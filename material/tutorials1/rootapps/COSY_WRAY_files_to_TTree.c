void COSY_WRAY_files_to_TTree()
{
  gROOT->Reset();
  char str[512];
  sprintf(str,"rm COSY_WRAY.txt; sleep 2"); system(str);
  sprintf(str,"cat WRAY.* > COSY_WRAY.txt"); system(str);
  //
  TFile *f = new TFile("COSY_WRAY.root","RECREATE");
  TTree *T = new TTree("ntuple","data from ascii file");
  Int_t nRead = T->ReadFile( "COSY_WRAY.txt","xf:af:yf:bf:tf:df");
  //Define Aliases and list them
  T->Write();
  //T->StartViewer(); //start window based analysis session

  int plot_method=2; //0=> note, 1=>separate canvases 2=> single canvas
  //
  if(plot_method==1){
   Int_t cH=600, cV=610;
   TCanvas *cyx = new TCanvas("cyx","cyx", 10,10, cH,cV);
   T->Draw("yf:xf");  //generate TGraph of variables
    TGraph *gryx=(TGraph*)cyx->GetListOfPrimitives()->FindObject("Graph");
    gryx->SetMarkerStyle(8);
   TCanvas *cax = new TCanvas("cax","cax", 50,50, cH,cV);
   T->Draw("af:xf");
    TGraph *grax=(TGraph*)cax->GetListOfPrimitives()->FindObject("Graph");
    grax->SetMarkerStyle(8);
   TCanvas *cby = new TCanvas("cby","cby", 50+cH,50, cH,cV);
   T->Draw("bf:yf");
    TGraph *grby=(TGraph*)cby->GetListOfPrimitives()->FindObject("Graph");
    grby->SetMarkerStyle(8);
  } else if (plot_method==2){
    TCanvas *c1 = new TCanvas("c1","c1", 10,10, 1200,480);
    c1->Divide(3,1);
    c1->cd(1);
    T->Draw("yf:xf");  //generate TGraph of variables
     TGraph *gryx=(TGraph*)c1->cd(1)->GetListOfPrimitives()->FindObject("Graph");
     gryx->SetMarkerStyle(8);
    c1->cd(2);
    T->Draw("af:xf");
     TGraph *grax=(TGraph*)c1->cd(2)->GetListOfPrimitives()->FindObject("Graph");
     grax->SetMarkerStyle(8);
    c1->cd(3);
    T->Draw("bf:yf");
     TGraph *grby=(TGraph*)c1->cd(3)->GetListOfPrimitives()->FindObject("Graph");
     grby->SetMarkerStyle(8);
  }
  
  //Optionals:
  //f->Close();
  return;
}


/* COSY tools needed for this method to be used

  {-- SR based methods -- SR based methods -- SR based methods -- }
   PROCEDURE WRAY_COL IU ;   {PRINTS RAY IN COLUMN FORM TO UNIT IU
     THE FIRST RAY IS THE REFERENCE
     Output format is easier to read by external apps.
     Example: COSY_WRAY_files_to_TTree.C is used to read all files that are 
      generated with this and called WRAY.??? }
      VARIABLE NR 1 ; VARIABLE LABL 1 8 ; VARIABLE I 1 ; VARIABLE J 1 ;
      NR := LENGTH(RAY(1)) ; IF NR=1 ; NR := 0 ; ENDIF ; {SEE CR AND SR}
      {WRITE IU '# number of rays:'&SF(NR,'(I8)') ;}
      IF NR>0 ;
         {WRITE IU '#X A Y B T D' ; leave out G Z }
         LOOP J 3 NR ; {skip reference particles}
           WRITE IU S(RAY(1)|J)&' '&S(RAY(2)|J)&' '&S(
           RAY(3)|J)&' '&S(RAY(4)|J)&' '&S(RAY(5)|J)&' '&S(RAY(6)|J) ;
         ENDLOOP ;
      ENDIF ;
      ENDPROCEDURE ;

*/