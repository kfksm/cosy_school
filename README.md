# COSY_school

- Repository for COSY_school in FRIB.
- [cosy91src](https://gitlab.msu.edu/portill2/cosy91src) (for MSU internal user)

# Material
- Course materials

# Documentation
- [Beam Physics Manual v9.1](https://gitlab.msu.edu/portill2/cosy91src/-/blob/master/examples_n_windows/COSYBeamMan91r.pdf) (for MSU internal user)
- [Programmer's Manual v9.1](https://gitlab.msu.edu/portill2/cosy91src/-/blob/master/examples_n_windows/COSYProgMan91.pdf) (for MSU internal user)

# Questions
- Use issue tracker for your question. [See example](https://gitlab.msu.edu/kfksm/cosy_school/-/issues/1).
